package com.my.coniex.rhbtest.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.my.coniex.rhbtest.model.Person;
import com.my.coniex.rhbtest.repository.PersonRepository;

@RestController
@RequestMapping("/persons")
public class PersonController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

	@Autowired
	PersonRepository personRepository;

	@PostMapping
	public ResponseEntity<Person> create(@RequestBody Person person) {
		LOGGER.info("Received Person :" + person.toString());
		Person created = personRepository.save(person);
		return new ResponseEntity<Person>(created, HttpStatus.CREATED);
	}

	@GetMapping
	public List<Person> getAll() {
		LOGGER.info("Get All Persons");
		return personRepository.findAll();
	}

	@GetMapping(value = "/getPersonByFirstName/{name}")
	public ResponseEntity<String> getPersonByFirstName(@PathVariable String name) {
		LOGGER.info(String.format("Find by %s", name));
		Person found =  personRepository.findByFirstName(name);
		if(found != null) {
			return new ResponseEntity<String>(name, HttpStatus.OK);
		}else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(value = "/{name}")
	public ResponseEntity<Person> update(@RequestBody Person person, HttpServletResponse res) {
		LOGGER.info("Received Person :" + person.toString());
		Person found = personRepository.findByFirstName(person.getFirstName());

		if (found != null) {
			found.setFirstName(person.getFirstName());
			found.setEmail(person.getEmail());
			found.setLastName(person.getLastName());
			person = personRepository.save(found);
		} else {
			return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{name}")
	public ResponseEntity<String> delete(@PathVariable String name, HttpServletResponse res) {
		LOGGER.info("Received Person :" + name.toString());
		Person found = personRepository.findByFirstName(name);
		if (found != null) {
			personRepository.delete(found);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(name, HttpStatus.OK);
	}
	

}