package com.my.coniex.rhbtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.coniex.rhbtest.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
	
	public Person findByFirstName(String name);
}